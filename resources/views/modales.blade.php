 <!--Modal-->
 <div class="modal fade" id="modalhtml" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Temario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul class="list-group">
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Introduccion a HTML
            <span class="badge bg-primary ">1</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Formatos de pagina
            <span class="badge bg-primary ">2</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Formatos de Texto
            <span class="badge bg-primary ">3</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Imagenes y listas
            <span class="badge bg-primary ">5</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Tablas y Formularios
            <span class="badge bg-primary ">6</span>
          </li>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            Orillas y frameworks
            <span class="badge bg-primary ">7</span>
          </li>
        </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>