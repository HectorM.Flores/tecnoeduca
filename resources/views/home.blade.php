@extends('app')


@section('content')
<!--Carousel-->
<main id="main">
    <div id="carousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active" data-interval="3000">
            <img src="images/img1.jpg" class="d-block w-100"  alt="First slide">
          </div>
          <div class="carousel-item " data-interval="3000">
            <img src="images/img2.jpg" class="d-block w-100"  alt="Second slide">
          </div>
          <div class="carousel-item " data-interval="3000">
            <img src="images/img3.jpg" class="d-block w-100"  alt="Third slide">
          </div>
        </div>
        <div class="overlay">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 offset-md-6 text-md-right text-cnter">
                        <h1>TecnoEduca</h1>
                        <p class="d-none d-md-block">Por fin disponible en linea, un evento que vale la pena compartir,
                        la primera experiencia online con profesionales que lleva el aprendizaje a otro nivel.
                        </p>
                        <a href="{{route('registro')}}" class="btn btn-outline-light">Quiero ser alumno</a>
                        <button class="btn btn-info">More info</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
</main>

<!--Cursos principales-->
<section id="cursos" class="mt-4">
    <div class="container">
        <div class="row">
            <div class="col text-center text-uppercase">
                <small>Conoce nuestros</small> <h2>Cursos</h2>        
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4 mb-4">  
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="images/html.jpg" alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">HTML</h5>
                      <p class="card-text">El Lenguaje encargado de estructurar las paginas web para despues empezar a diseñar y generar logica sobre ella,
                        aprende las bases para adentrarte en el mundo del desarollo de sitios web.
                      </p>
                      <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modalhtml">Ver curso</a>
                    </div>
                  </div> 
            </div> 
            <div class="col-12 col-md-6 col-lg-4 mb-4">  
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="images/javascript.jpg" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">JavaScript</h5>
                    <p class="card-text">El Lenguaje interpretado por el navegador mas popular en los ultimos 10 años, javascript con sus
                      potentes funciones revoluciono el front y back end en el mundo web.
                    </p>
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Ver curso</a>
                  </div>
                </div> 
            </div>     
            <div class="col-12 col-md-6 col-lg-4 mb-4">  
              <div class="card" style="width: 18rem;">
                  <img class="card-img-top" src="images/css.jpg" alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title">CSS</h5>
                    <p class="card-text">Que seria una pagina web sin los estilos, sin CSS no hay front end, crea una pagina web que te enamore a la vista
                      y añadele experiencia de usuario junto con javascript.
                    </p>
                    <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Ver curso</a>
                  </div>
                </div> 
            </div>
                
        </div>
    </div>    
</section>

<!--Clase online-->
<section id="Claseonline">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-12 pl-0 pr-0">
        <img src="images/joven.jpg" alt="">
      </div>
      <div class="col-lg-6 col-12 pb-4 pt-4">
        <h2>Clase de prueba online Agosto 2021</h2>
        <p>
          Registrate a la clase de prueba online rellena el formulario de contacto para que podamos tener mas
          informacion sobre las necesidades especificas y puedas concretar una clase gratuita de 30 minutos con el
          profesor que mejor se adapte a tus necesidades academicas.
        </p>
        <a href="" class="btn btn-outline-light">Registrarme</a>
      </div>
    </div>
  </div>
</section>

      <!-- SE MAESTRO -->
  
      <section id="se-maestro" class="pt-4 pb-2">
        <div class="container">
          <div class="row">
            <div class="col text-center ">
              <small class="text-uppercase">Conviertete en un</small>
              <h2>Maestro</h2>
  
            </div>
          </div>
  
          <div class="row">
            <div class="col text-center">
              Participa en el proceso de selección y forma parte del equipo  <abbr data-toggle="tooltip" title="Es el nombre de tu nueva familia.">TecnoEduca.</abbr> 
            </div>
          </div>
  
          <div class="row">
            <div class="col col-md-10 offset-md-1  col-lg-8 offset-lg-2 pt-2">
              <form>
                <div class="form-row">
                  <div class="col-12 col-md-6 form-group">
                    <input type="text" class="form-control" placeholder="Nombre">
                  </div>
                  <div class="col-12 col-md-6 form-group">
                    <input type="text" class="form-control" placeholder="Apellidos">
                  </div>
                </div>
  
                <div class="form-row">
                  <div class="col form-group">
                    <textarea name="text" class="form-control form-control-lg" placeholder="Sobre que tema te interesa impartir"></textarea>
                    <small>Incluye un título en tu descripción</small>
                  </div>
                </div>
  
                <div class="form-row">
                  <div class="col form-group">
                    <button type="button" class="btn btn-secondary btn-block">Enviar</button>
                  </div>
                </div>
  
  
              </form>
            </div>        
          </div>
  
        </div>
      </section>

   


@endsection

